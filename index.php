<?php

/**
 * Generates camelCase setters/getters for a class
 *
 * @TODO Future improvements will adjust doc comment (if it's incorrect) for existing setters and getters. Also pass filters for property
 * Add documentation via --help parameter and on arguments fail.
*/
error_reporting('E_NONE');
ini_set('display_errors', 0);
const FILE_SIZE_LIMIT = 1024 * 5 * 1000 * 1000;

$inputFile = empty($argv[1]) ? '' : $argv[1];
$projectRoot = empty($argv[2]) ? '' : $argv[2];

if (empty($inputFile)) {
	echo 'No input file specified' . "\n";
	die;
}
if (!file_exists($inputFile)) {
	echo 'Input file does not exist' . "\n";
	die;
}

if (is_dir($inputFile)) {
	echo 'Directory generator is not yet implemented' . "\n";
	die;
}
if (filesize($inputFile) > FILE_SIZE_LIMIT) {
	echo 'File size exceeds ' . round(FILE_SIZE_LIMIT / 1000 / 1000, 2) . ' Mb' . "\n";
	die;
}

$setterTemplate = '/**
 * Set %s
 *
 * @param %s$%s
 * @return %s
 */
public function set%s(%s$%s)
{
    $this->%s = $%s;

    return $this;
)';

$getterTemplate = '/**
 * Get %s%s
 */
public function get%s()
{
    return $this->%s;
}';

$content = file_get_contents($inputFile);
// die;
// $pattern = '/^(?P<phpTag>\<\?php)\s+(?:namespace[^;]+;)?(?:use[^;]+;)*\s+class\s+(?P<className>[\w_]+)\s{/usi';
// $pattern = '/^(?P<phpTag>\<\?php)\s+(namespace[^;]+;\s+)?(use[^;]+;\s+)class\s+(?P<className>[\w_]+)*/usi';
$pattern = '/(namespace (?P<namespace>[^;]+)(?:.*))?class\s(?P<className>[\w]+)/usi';
$errorMessageNoClass = 'Input file is not a php class';
preg_match($pattern, $content, $matches);
if (empty($matches['className'])){
	echo $errorMessageNoClass . "\n";
	die;
}
chdir($projectRoot);
include_once getcwd() . '/vendor/autoload.php';
require $inputFile;
$tmpFilename = tempnam(sys_get_temp_dir() . 'Generate');
file_put_contents($tmpFilename, $content);
$content = preg_replace('/use [\w\/]+;/usi', '', $content);
try {
    $reflector = new ReflectionClass($matches['namespace'] . '\\' . $matches['className']);
} catch (\Exception $e) {
    echo $errorMessageNoClass . "\n";
    die;
}
// var_dump($reflector);
$property = $reflector->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE);
$method = $reflector->getMethods();
$propertyName = [];
$methodName = [];
foreach ($property as $key => $value) {
    if ($value->isStatic()) {
        continue;
    }
    $propertyName[] = $value->name;
}
foreach ($method as $key => $value) {
    $methodName[] = $value->name;
}
$propertyMap = [];

foreach ($propertyName as $value) {
    $capitalizedProperty = ucwords($value);
    // die;
    $propertyMap[$value]['docComment'] = $reflector->getProperty($value)->getDocComment();
    if (in_array('set' . ucwords($capitalizedProperty), $methodName)) {
        $propertyMap[$value]['setter'] = true;
    } else {
        $propertyMap[$value]['setter'] = false;
    }
    if (in_array('get' . ucwords($capitalizedProperty), $methodName)) {
        $propertyMap[$value]['getter'] = true;
    } else {
        $propertyMap[$value]['getter'] = false;
    }
}
$setter = [];
$getter = [];

foreach ($propertyMap as $key => &$value) {
    if ($value['setter'] && $value['getter']) {
        continue;
    } 
    $setterTemplateParameters = [];
    $getterTemplateParameters = [];
    $capitalizedProperty = ucwords($key);
    $docDommentType = '';

    preg_match('/@var\s([^\s]+)/', $value['docComment'], $matches);

    $propertyType = empty($matches[1]) ? '' : $matches[1];
    
    $setterTemplateParameters = [];

    if (!$value['setter']) {
        $setterTemplateParameters[] = $setterTemplate;
        $setterTemplateParameters[] = $key;
        $setterTemplateParameters[] = empty($propertyType) ? '' : $propertyType . ' ';
        $setterTemplateParameters[] = $key;
        $setterTemplateParameters[] = $reflector->getShortName();
        $setterTemplateParameters[] = $capitalizedProperty;
        $setterTemplateParameters[] = empty($propertyType) ? '' : $propertyType . ' ';
        $setterTemplateParameters[] = $key;
        $setterTemplateParameters[] = $key;
        $setterTemplateParameters[] = $key;

        $value['setter'] = call_user_func_array('sprintf', $setterTemplateParameters);
    }

    
    if (!$value['getter']) {
        $getterTemplateParameters[] = $getterTemplate;
        $getterTemplateParameters[] = $key;
        if (!empty($propertyType)) {
            $docDommentType .= "\n" . ' *' . "\n" . ' * @return ' . $propertyType;
            $getterTemplateParameters[] = $docDommentType;
        } else {
            $getterTemplateParameters[] = '';
        }
        $getterTemplateParameters[] = $capitalizedProperty;
        $getterTemplateParameters[] = $key;

        $value['getter'] = call_user_func_array('sprintf', $getterTemplateParameters);
    }
}
$generatedContent = "\n";
foreach ($propertyMap as &$value) {
    if ($value['setter'] !== true) {
        $generatedContent .= "\n";
        $setterLines = explode("\n", $value['setter']);
        foreach ($setterLines as $line) {
            $generatedContent .= str_repeat(' ', 4) . $line . "\n";
        }
    }
    if ($value['getter'] !== true) {
        $generatedContent .= "\n";
        $getterLines = explode("\n", $value['getter']);
        foreach ($getterLines as $line) {
            $generatedContent .= str_repeat(' ', 4) . $line . "\n";
        }
    }
}
$content = preg_replace('/(\s*})\s*(?:\?\>)?\s*$/', $generatedContent . '}', $content);
unlink($tmpFilename);
echo $content;