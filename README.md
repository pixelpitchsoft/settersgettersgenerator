# README #

PHP class setters and getter generator

### What is this repository for? ###

* Generates input php file setters and getters and outputs the result to STDOUT
* v0.0.4

### How do I get set up? ###

* Example:
```shell
php index.php testApp.com/module/Task/src/Task/Entity/Task.php
```

### Contribution guidelines ###

* Automated tests
* Code review
* TODO list in `index.php`

### Who do I talk to? ###

* [Povilas B.](mailto:pbrilius@gmail.com)
* [Medardas M.](https://www.linkedin.com/in/medardas-mikolajunas-b9939986)